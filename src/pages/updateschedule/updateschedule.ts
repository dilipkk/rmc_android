import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { User } from '../../providers';
import { AlertController } from 'ionic-angular';

/**
 * Generated class for the UpdateschedulePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-updateschedule',
  templateUrl: 'updateschedule.html',
})
export class UpdateschedulePage {
date;
time;
id;
  constructor(private alertCtrl:AlertController,public navCtrl: NavController, public navParams: NavParams,private user:User,private toast:ToastController) {
  }

  ionViewDidLoad() {
    this.id=this.navParams.get('userid')
    console.log(this.id)
    console.log('ionViewDidLoad UpdateschedulePage');
  }
UpdateSchedule(){
  console.log(this.date)
  var datetime = this.date+ ' '+this.time

  console.log(this.time)
  var data ={
updatedtime:datetime,
orderid:this.id
}
console.log(data)
this.user.Updateorders(data).subscribe(res=>{
  console.log(res)
  // let toast = this.toast.create({
  //   message: 'order scheduled',
  //   duration: 3000,
  //   position: 'top'
  // });
  // toast.present();
  this.presentAlert('Re-Schdule Orders','Success')
    this.navCtrl.setRoot('ViewOrdersPage');
  // this.navParams.navigation()
})
}
presentAlert(title,subtitle) {
  let alert = this.alertCtrl.create({
    title:title,
    subTitle:subtitle,
    buttons: ['ok']
  });
  alert.present()
}
}
