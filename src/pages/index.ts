// The page the user lands on after opening the app and without a session
const userDetails = localStorage.getItem("userDetails");
const driver_id = localStorage.getItem("driver_id");
import { OrdersPage } from './list-order/list-order'

export var FirstRunPage
if (userDetails) {
    console.log('using sales engineer login');
    
    FirstRunPage = 'TabsPage';
}
else if (driver_id) {
    console.log('using driver login')
    FirstRunPage = OrdersPage;
}
else {
    console.log('redirect to login page')
    FirstRunPage = 'LoginPage'
}

// The main page the user will see as they use the app over a long period of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'ListMasterPage';
export const Tab2Root = 'SearchPage';
export const Tab3Root = 'SettingsPage';
