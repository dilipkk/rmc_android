import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchedulecallsPage } from './schedulecalls';

@NgModule({
  declarations: [
    SchedulecallsPage,
  ],
  imports: [
    IonicPageModule.forChild(SchedulecallsPage),
  ],
})
export class SchedulecallsPageModule {}
