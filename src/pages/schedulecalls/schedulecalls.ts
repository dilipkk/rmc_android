import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,AlertController } from 'ionic-angular';
import { User } from '../../providers';


/**
 * Generated class for the SchedulecallsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedulecalls',
  templateUrl: 'schedulecalls.html',
})
export class SchedulecallsPage {
		scheduleData: { [k: string]: any } = {};
		resp:any;
customerid:any;
    userDetails = JSON.parse(localStorage.getItem("userDetails"));
	  authKey = localStorage.getItem('authKey');
val:any;
getdatas:any;
type:any;
id:any;
types
updatedcall
  constructor(public navCtrl: NavController, public navParams: NavParams,public user: User,private alertCtrl: AlertController) {
  	this.val=this.navParams.get('val')
    this.updatedcall=this.navParams.get('update');
console.log(this.val)
console.log(this.userDetails)
if(this.val==2)
{
      this.types=this.navParams.get('type')
      this.type=this.navParams.get('val')


this.authKey= localStorage.getItem('authKey');

this.getdatas=this.navParams.get('resp');
console.log(this.getdatas)
this.scheduleData.company_name=this.getdatas.company_name;
  this.scheduleData.date=this.getdatas.date;
    this.scheduleData.time=this.getdatas.time;
  this.scheduleData.comments=this.getdatas.comments;
    this.customerid=this.getdatas.customer_id;
    // this.authKey= this.authKey;
    console.log(this.authKey)
this.id=this.userDetails['id'];
  // this.customerid=this.resp.customer_id;



}
else
{
   this.resp=this.navParams.get('resp');
    this.customerid=this.resp.customer_id;

    this.authKey= localStorage.getItem('authKey');

}


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulecallsPage');
  }

  SubmitSchedulepage(form) {
    if(this.val==2)
    {
      console.log(this.userDetails)
      console.log(this.userDetails.id)
      console.log(typeof this.userDetails.id)

console.log(this.userDetails.id)
var update = this.scheduleData.date +" "+this.scheduleData.time
console.log(update)
console.log(this.authKey)
if(this.updatedcall == false){


     var data={


     auth_key:this.authKey,
  user_id:this.userDetails.id,
  customer_id:this.getdatas.customer_id,
    company_name:this.getdatas.company_name,
    datetime : update,
    time : this.scheduleData.time,
    type:this.types,
    comments: this.scheduleData.comments,
    // call_id:this.id
     // customer_id:this.customerid
     }
  console.log(data)
 /* var scalls= [{
          data
          }]*/
              let datas = {"data":[data]}
console.log(datas)

    this.user.createCalls(datas).subscribe((resp:any) => {
      //this.navCtrl.push(MainPage);
      console.log(resp)
      let alert = this.alertCtrl.create({
        message: resp.msg,
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: data => {
              console.log('Cancel clicked');
            }
          }]
      });
      alert.present();
      if(resp.status == "success"){
              this.navCtrl.setRoot('TabsPage');

      } else {

      }
    }, (err) => {
        console.log(err)


    });
  }
  if(this.updatedcall == true){
  var userdet=   JSON.parse(localStorage.getItem("caller_id"))
  console.log(userdet)
    var upd_data={
    auth_key:this.authKey,
 user_id:this.userDetails.id,
 customer_id:this.getdatas.customer_id,
   company_name:this.getdatas.company_name,
   update_datetime : update,
   time : this.scheduleData.time,
   type:this.types,
   comments: this.scheduleData.comments,
    call_id:userdet




    }
let upd_datas = {"data":[upd_data]}
console.log(upd_datas)
    this.user.updateCalls(upd_datas).subscribe(res=>{
console.log(res)
// if(res['status'] == "success"){
// let alert = this.alertCtrl.create({
//   message: '',
//   buttons: [
//     {
//       text: 'OK',
//       role: 'cancel',
//       handler: data => {
//         console.log('Cancel clicked');
//       }
//     }]
// });
// alert.present();
// }
if(res['status'] == "success"){
  let alert = this.alertCtrl.create({
    message: ' Schedule Updated',
    buttons: [
      {
        text: 'OK',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      }]
  });
  alert.present();

        this.navCtrl.setRoot('TabsPage');

} else {
}
// }
}, (err) => {
  console.log(err)


});
  }
    }


  };

}
