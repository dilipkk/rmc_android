import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SchedulecallslistPage } from './schedulecallslist';

@NgModule({
  declarations: [
    SchedulecallslistPage,
  ],
  imports: [
    IonicPageModule.forChild(SchedulecallslistPage),
  ],
})
export class SchedulecallslistPageModule {}
