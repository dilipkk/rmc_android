import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,ModalController } from 'ionic-angular';
import { User } from '../../providers';

/**
 * Generated class for the SchedulecallslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-schedulecallslist',
  templateUrl: 'schedulecallslist.html',
})
export class SchedulecallslistPage {

	 requestBody:any;
  from_date:any;
  scheduleDetails:Array<any>=[];
  date:any;
    authKey = localStorage.getItem('authKey');

    userDetails = JSON.parse(localStorage.getItem("userDetails"));

  to_date:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public user: User,public modelCtrl:ModalController
) {
    // this.getBookingDetails1();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SchedulecallslistPage');
  }

   printVal()
  {

     console.log(this.from_date)
       console.log(this.to_date)
       if(this.to_date==undefined || this.to_date=="undefined")
       {

           //  this.getBookingDetails();
            let toast = this.toastCtrl.create({
          message: 'Please Select To Date',
          duration: 3000,
          position: 'top'
        });
        toast.present();
       }
       else if(this.from_date==undefined || this.from_date=="undefined")
       {
 let toast = this.toastCtrl.create({
          message: 'Please Select From Date',
          duration: 3000,
          position: 'top'
        });
        toast.present();
       }
       else
       {
          this.getBookingDetails();
       }
  }

  getBookingDetails(){
    var today = new Date();
    var dd = String(today.getDate());
    var mm = String(today.getMonth() + 1); //January is 0!
    var yyyy = today.getFullYear();
    this.date = dd + '-' + mm + '-' + yyyy;
    this.requestBody={
        "data": [{
     "user_id": this.userDetails.id,
     "auth_key":this.authKey,
/*     "user_id":"1",
*/      "from_date":this.from_date,
      "to_date":this.to_date,
      "type":"1",
      "page":"1",
    }]
}
    console.log(this.requestBody);
    console.log(JSON.stringify(this.requestBody));

    this.user.CallList(this.requestBody).subscribe((resp:any)=>{
      if(resp.status== "success"){
        this.scheduleDetails = resp.call_list
        console.log(this.scheduleDetails)
        var scdulecall = this.scheduleDetails
        this.scheduleDetails =[]
        for (let i = 0; i < scdulecall.length; i++) {
          if(scdulecall[i].reschedule_status != "yes")
          this.scheduleDetails.push(scdulecall[i])
        }
        // reschedule_status: "no"
      } else {
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        //location.reload();
      }
    });
  }


   getBookingDetails1(){
    var today = new Date();
    var dd = String(today.getDate());
    var mm = String(today.getMonth() + 1); //January is 0!
    var yyyy = today.getFullYear();
    this.date = dd + '-' + mm + '-' + yyyy;
    this.from_date = yyyy + '-' +"0"+ mm + '-' + dd;
    this.to_date= yyyy + '-' +"0"+ mm + '-' + dd;
     this.requestBody={
        "data": [{
     "auth_key":this.authKey,
      "user_id": this.userDetails.id,
      "from_date":this.date,
      "to_date":this.date,
      "type":"1",
      "page":"1"
    }]
    }
    console.log(this.requestBody);
    console.log(JSON.stringify(this.requestBody));

    this.user.CallList(this.requestBody).subscribe((resp:any)=>{
      if(resp.status== "success"){
        this.scheduleDetails=[]

        // this.scheduleDetails = resp.call_list
      for (let i = 0; i < resp.call_list.length; i++) {
        if(resp.call_list[i]['reschedule_status'] == 'no'){
            this.scheduleDetails.push(resp.call_list[i])
        }
      }
        // console.log(this.scheduleDetails[0]['reschedule_status'])

        console.log(this.scheduleDetails)
      } else {
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        //location.reload();
      }
    });
  }
UpdateSchedule(item){
  console.log(item)
  // var date = new Date()
localStorage.setItem('caller_id',item.call_id)
  var data ={
    customer_id:item.customer_id,
    company_name:item.company_name,
    customer_name:item.contact_name,
    email_address:item.customer_email,
    customer_mobile:item.customer_mobile,
    customer_address:item.customer_address,
    call_id:item.call_id
  }
  console.log(data)
  const model = this.modelCtrl.create('ItemCreatePage',{resp:data,})
  model.present()
}
}
