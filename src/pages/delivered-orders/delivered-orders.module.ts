import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeliveredOrdersPage } from './delivered-orders';

@NgModule({
  declarations: [
    DeliveredOrdersPage,
  ],
  imports: [
    IonicPageModule.forChild(DeliveredOrdersPage),
  ],
})
export class DeliveredOrdersPageModule {}
