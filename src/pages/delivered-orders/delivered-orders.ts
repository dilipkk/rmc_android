import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { User } from '../../providers';

/**
 * Generated class for the DeliveredOrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-delivered-orders',
  templateUrl: 'delivered-orders.html',
})
export class DeliveredOrdersPage {
userid;
from_date:any;
to_date:any;
deliveryorders:Array<any>=[]
suborders:Array<any>[]=[];
subordersview:Boolean[]=[]
  constructor(public navCtrl: NavController, public navParams: NavParams,private user:User,public toastCtrl: ToastController) {
  }

ngOnInit(){
  var x =localStorage.getItem('userDetails')
  var y =JSON.parse(x)
  console.log(y)
  this.userid = y.id
  var today = new Date();
  var dd = String(today.getDate());
  var mm = String(today.getMonth() + 1); //January is 0!
  var yyyy = today.getFullYear();

  this.from_date = yyyy + '-' +"0"+ mm + '-' + dd;
  this.to_date= yyyy + '-' +"0"+ mm + '-' + dd;
  var data ={
    // sales_uid:this.userid
    userid:this.userid,
    "from_date":this.from_date,
    "to_date":this.to_date,

  }
  this.user.get_delivery_orders(data).subscribe(res=>{
    console.log(res)
    this.deliveryorders = res['data']['completed'].reverse()
  })
}
  ionViewDidLoad() {
    // var x =localStorage.getItem('userDetails')
    // var y =JSON.parse(x)
    // console.log(y)
    // this.userid = y.id
    console.log('ionViewDidLoad DeliveredOrdersPage');
    // var data ={
    //   // sales_uid:this.userid
    //   userid:this.userid
    //
    // }

    // this.user.get_delivery_orders(data).subscribe(res=>{
    //   console.log(res)
    //   this.deliveryorders = res['data']['completed']
    // })
  }
viewsuborder(ind,itemid){
  console.log(ind,itemid)
  this.suborders[ind]=[]

  var data ={
    order_id: itemid
  }
  this.user.getdeliveredsuborders(data).subscribe(res=>{
console.log(res)
this.suborders[ind] = res['data']
for (let i = 0; i < this.deliveryorders.length; i++) {

  this.subordersview[i]=false;

}
  this.subordersview[ind]=true;
  })
}
printVal(){
  console.log(this.from_date)
    console.log(this.to_date)
    if(this.to_date==undefined || this.to_date=="undefined")
    {

        //  this.getBookingDetails();
         let toast = this.toastCtrl.create({
       message: 'Please Select To Date',
       duration: 3000,
       position: 'top'
     });
     toast.present();
    }
    else if(this.from_date==undefined || this.from_date=="undefined")
    {
let toast = this.toastCtrl.create({
       message: 'Please Select From Date',
       duration: 3000,
       position: 'top'
     });
     toast.present();
    }
    else
    {
      var data ={
        // sales_uid:this.userid
        userid:this.userid,
        "from_date":this.from_date,
        "to_date":this.to_date,

      }
      this.user.get_delivery_orders(data).subscribe(res=>{
        console.log(res)
        this.deliveryorders = res['data']['completed'].reverse()
      })
         // this.getViewOrdersDetails();
    }
  }
}
