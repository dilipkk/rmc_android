import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers';
import { MainPage } from '../';
import { OrdersPage } from '../list-order/list-order';
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account= {
    data:[{username:"",password:""}]
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public user: User,
    public toastCtrl: ToastController,
    public translateService: TranslateService) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLogin() {
    console.log(this.user);
    this.user.login(this.account).subscribe((resp:any) => {
      // this.navCtrl.push(MainPage);
      if(resp.status == "success"){   
         console.log('role')
      console.log(resp.user_info.user_role)
        localStorage.setItem("authKey",resp.auth_key);
        localStorage.setItem("branch_id",resp.user_info.branch_id);
        localStorage.setItem("userDetails",JSON.stringify(resp.user_info));
        localStorage.setItem("user_role",resp.user_info.user_role);
        if(resp.user_info.user_role == '3'){
          //this.navCtrl.setRoot('ViewOrdersPage'); 
          this.navCtrl.setRoot('TabsPage'); 
        }else{
          this.navCtrl.setRoot('TabsPage');
        }
      }
       else {
         console.log(this.account['data'])
         const login = {
           username:this.account['data'][0]['username'],
           password:this.account['data'][0]['password']
         }
         console.log(login)
        this.user.driverlogin(login).subscribe((resp:any)=>{
          console.log(resp)
        
          if(resp.status == "success"){
          console.log(resp)
          var temp = JSON.stringify(resp['data'].driver_details.id)
          localStorage.setItem('driver_id',temp)
          this.navCtrl.setRoot('OrdersPage', {param1: 'list-order'});
              // this.navCtrl.setRoot(OrdersPage);
          }
          else{
            let toastError = this.toastCtrl.create({
              message: 'Enter Username and Password',
              duration: 3000,
              position: 'top'
            });
            toastError.present();
          }
        })
        // let toastError = this.toastCtrl.create({
        //   message: 'Enter Username and Password',
        //   duration: 3000,
        //   position: 'top'
        // });
        // toastError.present();
      }
    }, (err) => {
      //this.navCtrl.push(MainPage);
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
