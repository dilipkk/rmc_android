import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddproductdetailPage } from './addproductdetail';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    AddproductdetailPage,
  ],
  imports: [
    FormsModule,ReactiveFormsModule,
    IonicPageModule.forChild(AddproductdetailPage),
  ],
})
export class AddproductdetailPageModule {}
