import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController,AlertController } from 'ionic-angular';
import { User } from '../../providers';

import {FormBuilder, FormGroup,FormArray,Validators} from '@angular/forms';

/**
 * Generated class for the AddproductdetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-addproductdetail',
  templateUrl: 'addproductdetail.html',
})
export class AddproductdetailPage {
  public addproduct:FormGroup;
deliver_data:FormArray;
		totquantity:any;
	split:any;

	// productData: { [k: string]: any }={}
  // productData: { [k: string]: any }=[]
  productDatas: { [k: string]: any }={}
  addquantity:number[]=[]
/*    data: { [k: string]: any }={}
*/
  /* = {"billing_name":"test","billing_address":"test","contact_number":"233223","product_name":"test","price":"23","quantity":"23","ProductDeliveryDate":"2019-04-19","deliver_time":"15:12","ProductDeliveryAddress":"teste","totquantity":"32","deliver_data":[{"deliver_quantity":"102","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"},{"deliver_quantity":"103","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"},{"deliver_quantity":"101","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"}]};
	*/

   // productData: { [k: string]: any } = {"billing_name":"test","billing_address":"test","contact_number":"233223","product_name":"test","price":"23","quantity":"23","ProductDeliveryDate":"2019-04-19","deliver_time":"15:12","ProductDeliveryAddress":"teste","totquantity":"32","deliver_data":[{"deliver_quantity":"102","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"},{"deliver_quantity":"103","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"},{"deliver_quantity":"101","deliver_address":"Sample Address","deliver_contact_number":"98765410011","deliver_date":"2019-02-10","deliver_time":"06:11:13","status":"placed"}]};

  authKey = localStorage.getItem('authKey');
  branch_id = localStorage.getItem('branch_id');


bill_type:any;
resp:any;
    userDetails = JSON.parse(localStorage.getItem("userDetails"));
    customer_id:any;
choosetype
updatedcall;
formSubmitAttempt:Boolean= false;
duration=[1,2,3,4,5,6,7,8,9,10,11,12]
  constructor(public navCtrl: NavController, public navParams: NavParams,  public user: User,public toastCtrl: ToastController,private alertCtrl: AlertController ,public fb:FormBuilder) {
  	 // 	console.log(this.productData.quantity)
this.resp=this.navParams.get('resp');
this.choosetype=this.navParams.get('type');
this.updatedcall=this.navParams.get('update');

console.log(this.resp)
console.log(this.userDetails)
  this.customer_id=this.resp.customer_id;
  this.productDatas.billing_name= this.resp.customer_name ;
  this.productDatas.billing_address= this.resp.customer_address;
  this.productDatas.contact_number= this.resp.customer_mobile;

  this.addproduct = this.fb.group({
    company_name:['',Validators.required],
    billing_name:['',Validators.required],
    billing_address:['',Validators.required],
    contact_number:['',[Validators.required]],
    // product_name:['',Validators.required],
    customer_email:['',Validators.required],
    // product_name:[''],

    price:['',Validators.required],
    quantity:['',Validators.required],
    product_name:['',Validators.required],
    bill_type:['',Validators.required],
    gst_no:[''],
    // ProductDeliveryDate:['',Validators.required],
    // ProductDeliveryAddress:['',Validators.required],
    deliver_data:this.fb.array([this.createorder()])

    // question1type:['',Validators.required],
    // question1options:this.fb.array([this.createoption1()]),/
  })
  console.log(this.addproduct)
    this.addproduct.controls['company_name'].setValue(this.resp.company_name)
    this.addproduct.controls['customer_email'].setValue(this.resp.customer_email)
  this.addproduct.controls['billing_name'].setValue(this.resp.customer_name)
  this.addproduct.controls['billing_address'].setValue(this.resp.customer_address)
    this.addproduct.controls['contact_number'].setValue(this.resp.customer_mobile)
  console.log(this.addproduct)
// this.productDatas.deliver_data=[{"deliver_contact_name":"","deliver_quantity":"","deliver_address":"","deliver_contact_number":"","deliver_date":"","deliver_time":"","status":""}]
// {"deliver_contact_name":"","deliver_quantity":"","deliver_address":"","deliver_contact_number":"","deliver_date":"","deliver_time":"","status":""}]
// console.log(this.productDatas.deliver_data)
// this.productDatas.deliver_data.push({
//
//   "deliver_contact_name":"",  "deliver_quantity":"","deliver_address":"","deliver_contact_number":"","deliver_date":"","deliver_time":"","status":""
//   });
// console.log(this.productDatas.deliver_data)
}
ngoninit(){
   this.initForm();

  console.log(this.productDatas.deliver_data)
  console.log(this.addproduct)
  this.addproduct.controls['billing_name'].setValue(this.resp.customer_name)
  this.addproduct.controls['billing_address'].setValue(this.resp.customer_address)
console.log(this.addproduct)
this.formSubmitAttempt=false;
}
initForm(){

}
createorder(){
  // createorder(){
  return this.fb.group({
      deliver_date:['',Validators.required],
      deliver_time:['',Validators.required],
      deliver_quantity:['',Validators.required],
      deliver_contact_name:['',Validators.required],
      order_duration:['',Validators.required],
      machine_requirement:['',Validators.required],
      deliver_contact_number:['',[Validators.required]],
      deliver_address:['',Validators.required]
    });

}
get add(){
  return <FormArray>this.addproduct.get('deliver_data')
}
dummyarray:Array<any>=[]
total:number = 0
checkblur(i,value){
  console.log(i,value)
  // this.addquantity[i] = 0;
  this.addquantity[i]= parseInt (value);
console.log(this.add)
  this.total = 0
  for (let i = 0; i < this.addquantity.length; i++) {
    console.log(this.addquantity[i])
      this.total =this.total + this.addquantity[i]
      console.log(this.total)
  }
  console.log("dsflakhdslk")
}
addorder(i){


  console.log(this.addproduct.get('deliver_data')['controls'])

  this.dummyarray.push()

  this.add.push(this.createorder())

}
remocesplit(i,length){
  if(length != 1 ){
  console.log(length)
    this.addquantity[i]= 0
    this.total = 0
    for (let i = 0; i < this.addquantity.length; i++) {
      console.log(this.addquantity[i])
        this.total =this.total + this.addquantity[i]
        console.log(this.total)
    }
    this.add.removeAt(i)
  }
  if(length == 1){
    let alert = this.alertCtrl.create({
      message: 'You Cannot remove ',

    });
    alert.present();
  }
}
  splitQty(){
    console.log("e")

  }

  remove(index){
    console.log(index);

  }

  onSelectChangelocation(event: any)
  {
  	console.log(event)

  }

  OpenProductaddpage(data){
    console.log("Hai Sherap");
    this.formSubmitAttempt =true
    // if (this.addproduct.controls.product_name.value.length == 0) {
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter  Product Name',
    //
    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.price.value.length == 0) {
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Product Rate',
    //
    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.quantity.value.length == 0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Select Total Quantity',
    //
    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.bill_type.value.length == 0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Billing Address',
    //
    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_date.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Date',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_time.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Time',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.order_duration.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Order Duration',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_quantity.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Quantity',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_address.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Delivery Address',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_contact_name.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Delivery Name',

    //   });
    //   alert.present();
    // }
    // else if (this.addproduct.controls.deliver_data.controls[0].controls.deliver_contact_number.value.length==0){
    //   let alert = this.alertCtrl.create({
    //     message: 'Please Enter Contact Number',

    //   });
    //   alert.present();
    // }


 if(this.updatedcall != true){


       if(this.addproduct.valid   && this.addproduct.controls.quantity.value == this.total  ){


      console.log(this.addproduct.value)
        data.user_id= this.userDetails.id,
       data.auth_key=this.authKey
       data.customer_id=this.customer_id
       var temp ={
         user_id:this.userDetails.id,
         auth_key:this.authKey,
       }
       var temp1={
         customer_id:this.customer_id

       }
       var obj = Object.assign(temp,temp1,this.addproduct.value)
       obj.branch_id = this.branch_id
           console.log(JSON.stringify(obj));
      let datas = {"data":[obj]}
    console.log(datas)
    console.log(JSON.stringify(datas));
  var   date = new Date();
    var day = date.getDate();
  var month = (date.getMonth()+1);
  var year = date.getFullYear();
  var hours =date. getHours()
  var min = date.getMinutes()
  var datetime= year+'-'+month+'-'+day +" "+ hours+':'+min
    var time= hours+':'+min
  console.log(day,month,year)
    var create_new_calls={
      user_id:this.userDetails.id,
        auth_key:this.authKey,
      customer_id:this.customer_id,
      company_name:this.addproduct.value.company_name,
      comments:"",
      datetime : datetime,
      time:time,
      type:this.choosetype
    }
    let createcall = {"data":[create_new_calls]}
    this.user.createCalls(createcall).subscribe((resp:any) => {
      console.log(resp)
      if(resp.status="success"){
        this.user.createOrder(datas).subscribe((resp:any) => {
          console.log(resp)
          let alert = this.alertCtrl.create({
            message: resp.msg,
            buttons: [
              {
                text: 'OK',
                role: 'cancel',
                handler: data => {
                  console.log('Created ');
                }
              }]
          });
          alert.present();

        if(resp.status == "success"){
        this.navCtrl.setRoot('TabsPage');
        } else {
          let toastError = this.toastCtrl.create({
            message: resp.msg,
            duration: 3000,
            position: 'top'
          });
          toastError.present();
        }
      }, (err) => {
          console.log(err)


      });
    }
    else{
      console.log("new calls are not crated")
      let alert = this.alertCtrl.create({
        message: 'New Calls  Not Created',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: data => {
              console.log('Created ');
            }
          }]
      });
      alert.present();
    }
    });

      }
      else{
        // let alert = this.alertCtrl.create({
        //   message: 'Please Enter  Other Fields ',
        //
        // });
        // alert.present();
      }
    }
   else if(this.updatedcall == true){
      if(this.addproduct.valid   && this.addproduct.controls.quantity.value == this.total  ){


      console.log(this.addproduct.value)
        data.user_id= this.userDetails.id,
       data.auth_key=this.authKey
       data.customer_id=this.customer_id
       var temp ={
         user_id:this.userDetails.id,
         auth_key:this.authKey,
       }
       var temp1={
         customer_id:this.customer_id

       }
       var obj = Object.assign(temp,temp1,this.addproduct.value)
       obj.branch_id = this.branch_id
           console.log(JSON.stringify(obj));
      let datas = {"data":[obj]}
    console.log(datas)
    console.log(JSON.stringify(datas));
  var   date = new Date();
    var day = date.getDate();
  var month = (date.getMonth()+1);
  var year = date.getFullYear();
  var hours =date. getHours()
  var min = date.getMinutes()
  var datetime= year+'-'+month+'-'+day +" "+ hours+':'+min
    var time= hours+':'+min
  console.log(day,month,year)
    var update_new_calls={
      // user_id:this.userDetails.id,
      //   auth_key:this.authKey,
      // customer_id:this.customer_id,
      // company_name:this.addproduct.value.company_name,
      // comments:"",
      // datetime : datetime,
      // time:time,
      // type:this.choosetype
      auth_key:this.authKey,
      user_id:this.userDetails.id,
      customer_id:this.customer_id,
      company_name:this.addproduct.value.company_name,
      update_datetime : datetime,
      time : time,
      type:"2",
      comments: "",
      call_id:this.resp.call_id
    }
    let createcall = {"data":[update_new_calls]}

    this.user.updateCalls(createcall).subscribe((resp:any) => {
      console.log(resp)
      if(resp.status="success"){
        this.user.createOrder(datas).subscribe((resps:any) => {
          console.log(resp)
            if(resps.status == "success"){

          let alert = this.alertCtrl.create({
            message: resp.msg,
            buttons: [
              {
                text: 'OK',
                role: 'cancel',
                handler: data => {
                  console.log('Created ');
                }
              }]
          });
          alert.present();

          this.navCtrl.setRoot('TabsPage');

}
      else {
          let toastError = this.toastCtrl.create({
            message: resp.msg,
            duration: 3000,
            position: 'top'
          });
          toastError.present();
        }
      }, (err) => {
          console.log(err)


      });
    }
    else{
      console.log("new calls are not crated")
    }
    });

      }
      else{
        // let alert = this.alertCtrl.create({
        //   message: 'Please Enter  Other Fields ',
        //
        // });
        // alert.present();
      }
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddproductdetailPage');
  }

  SubmitSchedulepage = function (form) {
     var data = this.productData;
     console.log(this.productData.deliver_data)
     data.auth_key=this.authKey;
     data.bill_type=this.bill_type
      data.user_id= this.userDetails.id,
     data.auth_key=this.authKey,

  console.log(data)

  };
onSelectChangemcube(e){
  console.log(e)
}
}
