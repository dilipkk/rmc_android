import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { User } from '../../providers';

/**
 * Generated class for the FailedcalllistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-failedcalllist',
  templateUrl: 'failedcalllist.html',
})
export class FailedcalllistPage {
	 requestBody:any;
  from_date:any;
  to_date:any;
  scheduleDetails:any;
  date:any;
    authKey = localStorage.getItem('authKey');

    userDetails = JSON.parse(localStorage.getItem("userDetails"));


  constructor(public navCtrl: NavController, public navParams: NavParams,public user: User,public toastCtrl: ToastController) {

// this.getBookingDetails1();
  }
ngOnInit(){
  var today = new Date();
  var dd = String(today.getDate());
  var mm = String(today.getMonth() + 1); //January is 0!
  var yyyy = today.getFullYear();
  this.date = dd + '-' + mm + '-' + yyyy;
  this.from_date = yyyy + '-' +"0"+ mm + '-' + dd;
  this.to_date= yyyy + '-' +"0"+ mm + '-' + dd;
  this.requestBody={
      "data": [{
   "user_id": this.userDetails.id,
   "auth_key":this.authKey,
/*     "user_id":"1",
*/      "from_date":this.from_date,
    "to_date":this.to_date,
    "type":"3",
    "page":"1",
  }]
}
  console.log(this.requestBody);
  this.user.CallList(this.requestBody).subscribe((resp:any)=>{
    if(resp.status== "success"){
      this.scheduleDetails = resp.call_list
      console.log(this.scheduleDetails)
    } else {
      let toast = this.toastCtrl.create({
        message: resp.message,
        duration: 3000,
        position: 'top'
      });
      toast.present();
      //location.reload();
    }
  });
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad FailedcalllistPage');
  }

   printVal()
  {

     console.log(this.from_date)
       console.log(this.to_date)
       if(this.to_date==undefined || this.to_date=="undefined")
       {

           //  this.getBookingDetails();
            let toast = this.toastCtrl.create({
          message: 'Please Select To Date',
          duration: 3000,
          position: 'top'
        });
        toast.present();
       }
       else if(this.from_date==undefined || this.from_date=="undefined")
       {
 let toast = this.toastCtrl.create({
          message: 'Please Select From Date',
          duration: 3000,
          position: 'top'
        });
        toast.present();
       }
       else
       {
          this.getBookingDetails();
       }
  }

  getBookingDetails(){
    var today = new Date();
    var dd = String(today.getDate());
    var mm = String(today.getMonth() + 1); //January is 0!
    var yyyy = today.getFullYear();
    this.date = dd + '-' + mm + '-' + yyyy;
    this.requestBody={
        "data": [{
     "user_id": this.userDetails.id,
     "auth_key":this.authKey,
/*     "user_id":"1",
*/      "from_date":this.from_date,
      "to_date":this.to_date,
      "type":"3",
      "page":"1",
    }]
}
    console.log(this.requestBody);
    this.user.CallList(this.requestBody).subscribe((resp:any)=>{
      if(resp.status== "success"){
        this.scheduleDetails = resp.call_list
        console.log(this.scheduleDetails)
      } else {
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        //location.reload();
      }
    });
  }


   getBookingDetails1(){
    var today = new Date();
    var dd = String(today.getDate());
    var mm = String(today.getMonth() + 1); //January is 0!
    var yyyy = today.getFullYear();
    this.date = dd + '-' + mm + '-' + yyyy;
     this.requestBody={
         "data": [{
     "auth_key":this.authKey,
      "user_id": this.userDetails.id,
      "from_date":this.date,
      "to_date":this.date,
      "type":"3",
      "page":"1"
    }]
    }
    console.log(this.requestBody);
    this.user.CallList(this.requestBody).subscribe((resp:any)=>{
      if(resp.status== "success"){
        this.scheduleDetails = resp.call_list
        console.log(this.scheduleDetails)
      } else {
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        //location.reload();
      }
    });
  }

}
