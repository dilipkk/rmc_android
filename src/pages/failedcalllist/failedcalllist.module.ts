import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FailedcalllistPage } from './failedcalllist';

@NgModule({
  declarations: [
    FailedcalllistPage,
  ],
  imports: [
    IonicPageModule.forChild(FailedcalllistPage),
  ],
})
export class FailedcalllistPageModule {}
