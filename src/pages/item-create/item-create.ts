import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { IonicPage, NavController, ViewController ,ModalController,NavParams} from 'ionic-angular';
import { User } from '../../providers';
import { AlertController,Keyboard  } from 'ionic-angular';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { IonicSelectableComponent } from 'ionic-selectable';
@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage {
  @ViewChild('fileInput') fileInput;
port;
 ports:Array<any>=[];
  isReadyToSave: boolean;
  authKey = localStorage.getItem('authKey');
  item: any;
  resp:any;
  form: FormGroup;
  updatelength:number=0;
  customer_id;
callid
  userdetail =JSON.parse(localStorage.getItem('userDetails'));
  customer_already:Boolean=false;
 private list: any[] = []
  constructor(private keyboard:Keyboard,private _sanitizer: DomSanitizer,private alertCtrl: AlertController,public navCtrl: NavController, public viewCtrl: ViewController,public navParams: NavParams,public modalCtrl: ModalController,formBuilder: FormBuilder, public camera: Camera,public user: User,) {
    	this.resp=this.navParams.get('resp');

      console.log(this.callid)
console.log(this.updatelength)

this.ports = [
    { id: 1, name: 'Tokai' },
    { id: 2, name: 'Vladivostok' },
    { id: 3, name: 'Navlakhi' }
  ];


    this.form = formBuilder.group({
      auth_key:[this.authKey],
      company_name: ['', Validators.required],
      customer_name: ['', Validators.required],
      email: [''],
      mobile: ['', [Validators.required, Validators.maxLength(19)]],
      address: ['', Validators.required],
      type: ['', Validators.required],
      status:["1"],
      map_location:[""]

    });
    if(this.resp != undefined){
      console.log(this.resp)
      this.customer_already=true
      console.log(Object.keys(this.resp).length)
      this.updatelength = Object.keys(this.resp).length
        // this.form.controls['company_name'].setValue(this.resp.company_name)
        this.customer_id = this.resp.customer_id
        this.input = this.resp.company_name
      this.form.controls['customer_name'].setValue(this.resp.customer_name)
      this.form.controls['email'].setValue(this.resp.email_address)
      this.form.controls['mobile'].setValue(this.resp.customer_mobile)
      this.form.controls['address'].setValue(this.resp.customer_address)
      this.form.controls['type'].setValue(this.resp.type)



      // this.form.controls['type'].setValue(this.resp.type)

    }
    console.log(this.form)

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
    this.user.getallcompany().subscribe(res=>{
      this.list = (res['data']).reverse();
      console.log( this.list)
    })
  }

  ionViewDidLoad() {
this.user.getallcompany().subscribe(res=>{
  this.list = (res['data']).reverse();
  console.log( this.list)
})
  }

  public input: string = '';
  public countries: string[] = [];
add(item: string) {

    this.input = item['company_name'];
    if(this.resp == undefined && item['company_name'] != '' && item['company_name'] != null && item['company_name'] != undefined && item['company_name']){
      var data={
  company_keyword:item['company_name']
      }
      this.user.getcompanydetailsbyname(data).subscribe(res=>{
        console.log(res['data'])
        // console.log(res['data'][0]['company_name'])
        if(res['status'] == "success" && res['data'].length !=0){
          this.customer_id =res['data'][0]['cust_id']
          this.customer_already=true
          // this.form.controls['company_name'].setValue(res['data'][0]['company_name'])
        this.form.controls['customer_name'].setValue(res['data'][0]['customer_name'])
        this.form.controls['email'].setValue(res['data'][0]['customer_email'])
        this.form.controls['mobile'].setValue(JSON.parse(res['data'][0]['mobile']))
        this.form.controls['address'].setValue(res['data'][0]['address'])
      }
      else{
        this.customer_already=false

        this.form.controls['customer_name'].setValue(null)
        // this.form.controls['contact_name'].setValue(null)
        this.form.controls['customer_name'].setValue(null)
        this.form.controls['email'].setValue(null)
        this.form.controls['mobile'].setValue(null)
        this.form.controls['address'].setValue(null)
      }
    });
  }
  else{
    this.customer_already=false

    this.form.controls['customer_name'].setValue(null)
    // this.form.controls['contact_name'].setValue(null)
    this.form.controls['customer_name'].setValue(null)
    this.form.controls['email'].setValue(null)
    this.form.controls['mobile'].setValue(null)
    this.form.controls['address'].setValue(null)
  }
    if (!this.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
      return;
    }
    this.countries = this.list.filter(item =>( item['company_name']).toUpperCase().includes((this.input).toUpperCase()));
    this.countries = [];
  }

  removeFocus() {
    this.keyboard.close();
  }
//autocomplete data fetch by type keys
  search() {
    console.log(this.input)
    console.log(typeof this.input)

    if(this.resp == undefined && this.input != '' && this.input != null && this.input != undefined && this.input){
      var data={
  company_keyword:this.input
      }
      this.user.getcompanydetailsbyname(data).subscribe(res=>{
        console.log(res['data'])

  this.customer_already=false
             this.form.controls['customer_name'].setValue(null)
             this.form.controls['email'].setValue(null)
             this.form.controls['mobile'].setValue(null)
             this.form.controls['address'].setValue(null)

        if(res['status'] == "success" && res['data'].length !=0){

    if (!this.input.trim().length || !this.keyboard.isOpen()) {
      this.countries = [];
      return;
    }

    }
    });
  }
  if(this.input){
    console.log(this.input.toLocaleUpperCase())

    // this.countries = this.list.filter(item =>( item['company_name']).toUpperCase().includes(this.input.toUpperCase()));
    this.countries = this.list.filter(item =>( item['company_name']).toLocaleUpperCase().includes(this.input.toLocaleUpperCase()));

  }
  }
  submit(){
    console.log(this.customer_already)
    // console.log(this.customer_already)

if(this.customer_already == false){
  console.log("new customer")
  var create_cutomer ={
  company_name:this.form.value.company_name,
  customer_name:this.form.value.customer_name,
  contact_name:this.form.value.customer_name,
  auth_key:this.form.value.auth_key,
  mobile:this.form.value.mobile,
  address:this.form.value.address,
  map_location:this.form.value.map_location,
  customer_email:this.form.value.email,
  status:this.form.value.status,
  userid:this.userdetail.id
  }
  let data = {"data":[create_cutomer]}
  // if(!this.customer_already){
  this.user.createCustomer(data).subscribe((resp:any) => {
    console.log(resp);
    if(resp.status="success"){
        this.customer_id = resp.customer_id;
        this.callsfunction();
    }
    else{
      console.log("create customer error")
    }
  })
  // }
  }
  else{
    console.log("Already customer")

    this.callsfunction();

  }
}

callsfunction(){
    if(this.updatelength == 0){
if(this.form.value.type == "2"){
  var response = {
    company_name:this.form.value.company_name,
    customer_id:this.customer_id,
    customer_name :this.form.value.customer_name,
    customer_address:this.form.value.address,
    customer_mobile:this.form.value.mobile,
    customer_email:this.form.value.email
  }
    this.navCtrl.push('AddproductdetailPage',{resp:response,val:2,type:2,update:false});
}
if(this.form.value.type == "1"){
  var response = {
    company_name:this.form.value.company_name,
    customer_id:this.customer_id,
    customer_name :this.form.value.customer_name,
    customer_address:this.form.value.address,
    customer_mobile:this.form.value.mobile,
    customer_email:this.form.value.email
  }
    this.navCtrl.push('SchedulecallsPage',{resp:response,val:2,type:1,update:false});

}
if(this.form.value.type == "3"){

  let alert = this.alertCtrl.create({
    title:'Cold Calls',
    subTitle:'Want to create Cold Call ?',
    // buttons: ['ok']
    buttons: [
       {
         text: 'No',
         role: 'cancel',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Confirm Cancel: blah');
         }
       }, {
         text: 'Yes',
         handler: () => {
           var   date = new Date();
             var day = date.getDate();
           var month = (date.getMonth()+1);
           var year = date.getFullYear();
           var hours =date. getHours()
           var min = date.getMinutes()
           var datetime= year+'-'+month+'-'+day +" "+ hours+':'+min
             var time= hours+':'+min
           console.log(day,month,year)
             var cold_calls ={
               user_id:this.userdetail.id,
               auth_key:this.authKey,
               customer_id:this.customer_id,
               company_name:this.form.value.company_name,
               comments:"",
               datetime : datetime,
               time:time,
               type:this.form.value.type
             }
             let createcall = {"data":[cold_calls]}
             this.user.createCalls(createcall).subscribe((resp:any) => {
               console.log(resp)
               if(resp.status="success"){
                 this.presentAlert()


                 console.log('Confirm Okay');
               }
             });
         }
       }
     ]
  });
 alert.present()
}
}

//upadate calls
else{
  if(this.form.value.type == "2"){
    var responses = {
      company_name:this.form.value.company_name,
      customer_id:this.customer_id,
      customer_name :this.form.value.customer_name,
      customer_address:this.form.value.address,
      customer_mobile:this.form.value.mobile,
      customer_email:this.form.value.email,
      call_id:this.resp.call_id
    }
      this.navCtrl.push('AddproductdetailPage',{resp:responses,val:2,type:2,update:true});
  }
  if(this.form.value.type == "1"){
    console.log( this.resp)
    var response = {
      company_name:this.form.value.company_name,
      customer_id:this.customer_id,
      customer_name :this.form.value.customer_name,
      customer_address:this.form.value.address,
      customer_mobile:this.form.value.mobile,
      customer_email:this.form.value.email,

    }
      this.navCtrl.push('SchedulecallsPage',{resp:response,val:2,type:1,update:true});

  }
  if(this.form.value.type == "3"){

    let alert = this.alertCtrl.create({
      title:'Cold Calls',
      subTitle:' Update to Cold Call ?',
      // buttons: ['ok']
      buttons: [
         {
           text: 'No',
           role: 'cancel',
           cssClass: 'secondary',
           handler: (blah) => {
             console.log('Confirm Cancel: blah');
           }
         }, {
           text: 'Yes',
           handler: () => {
             var   date = new Date();
               var day = date.getDate();
             var month =( date.getMonth()+1);
             var year = date.getFullYear();
             var hours =date. getHours()
             var min = date.getMinutes()
             var datetime= year+'-'+month+'-'+day +" "+ hours+':'+min
               var time= hours+':'+min
             console.log(day,month,year)

               var cold_calls={
                 user_id:this.userdetail.id,
                 auth_key:this.form.value.auth_key,
                 customer_id:this.customer_id,
                 company_name:this.form.value.company_name,
                   comments:"",
                   update_datetime : datetime,
                    time:time,
                   type:this.form.value.type,
                   call_id:this.resp.call_id
               }
               let createcall = {"data":[cold_calls]}
               this.user.updateCalls(createcall).subscribe((resp:any) => {
                 console.log(resp)
                 if(resp.status="success"){
                   this.presentAlert()


                   console.log('Confirm Okay');
                 }
               });
           }
         }
       ]
    });
   alert.present()
  }
}
}


  presentAlert() {
    let alert = this.alertCtrl.create({
        title: 'Cold Calls',
        subTitle: 'Cold call success',
        buttons:[
          {
            text: 'ok',
            role: 'ok',
            handler: () => {
            this.navCtrl.setRoot('ListMasterPage');
            }
          }
        ]
      });
      alert.present();

}
portChange(event: {
   component: IonicSelectableComponent,
   value: any
 }) {
   console.log(this.port)
   console.log('port:', event.value);
 }
//   sync  presentAlert() {
//   const alertController = document.querySelector('ion-import { AlertController } from '@ionic/angular';
// -controller');
//   await alertController.componentOnReady();
//
//   const alert = await alertController.create({
//     header: 'Alert',
//     subHeader: 'Subtitle',
//     message: 'This is an alert message.',
//     buttons: ['OK']
//   });
//   return await alert.present();
// }
  getPicture() {
    if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    if (!this.form.valid) { return; }
    this.viewCtrl.dismiss(this.form.value);
  }
  entername(e){
    console.log(e.target.value)
    var data={
company_keyword:e.target.value
    }
  if(this.resp == undefined){
    this.user.getcompanydetailsbyname(data).subscribe(res=>{
      console.log(res['data'])
      // console.log(res['data'][0]['company_name'])

      if(res['status'] == "success" && res['data'].length !=0){
        this.customer_already=true
          this.customer_id =res['data'][0]['cust_id']
        // this.form.controls['company_name'].setValue(res['data'][0]['company_name'])
      this.form.controls['customer_name'].setValue(res['data'][0]['customer_name'])
      this.form.controls['email'].setValue(res['data'][0]['customer_email'])
      this.form.controls['mobile'].setValue(JSON.parse(res['data'][0]['mobile']))
      this.form.controls['address'].setValue(res['data'][0]['address'])
    }
    else{
      this.customer_already=false

      this.form.controls['customer_name'].setValue(null)
      this.form.controls['customer_name'].setValue(null)
      this.form.controls['email'].setValue(null)
      this.form.controls['mobile'].setValue(null)
      this.form.controls['address'].setValue(null)
    }
    });
  }
}
}
