import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GetstaticsPage } from './getstatics';

@NgModule({
  declarations: [
    GetstaticsPage,
  ],
  imports: [
    IonicPageModule.forChild(GetstaticsPage),
  ],
})
export class GetstaticsPageModule {}
