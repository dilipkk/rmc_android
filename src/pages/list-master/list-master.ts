import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController } from 'ionic-angular';

import { Item } from '../../models/item';
import { Items } from '../../providers';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage {
  currentItems: Item[];
  role_id: "";

  constructor(public navCtrl: NavController, public items: Items, public modalCtrl: ModalController) {
    this.currentItems = this.items.query();
    this.role_id = localStorage.getItem('user_role');
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
  }

  /**
   * Routes
   */
  routePage(page) {
    if (page == 'AddproductdetailPage') {
      this.navCtrl.push('SchedulecallsPage');
    }
    else if (page == 'schedule') {
      const modal = this.modalCtrl.create('SchedulecallslistPage');
      modal.present();
    }

    else if (page == 'dashboard') {
      const modal = this.modalCtrl.create('SchedulecallsPage');
      modal.present();
    }
    else if (page == 'cold') {
      const modal = this.modalCtrl.create('FailedcalllistPage');
      modal.present();
    }
    else if (page == 'ViewOrdersPage') {
      const modal = this.modalCtrl.create('ViewOrdersPage');
      modal.present();
    }
    else if (page == 'statics') {
      const modal = this.modalCtrl.create('GetstaticsPage');
      modal.present();

    }
    else if (page == 'new') {
      this.navCtrl.push('ItemCreatePage');


    }
    else if (page == 'delivered') {
      this.navCtrl.push('DeliveredOrdersPage');

    }




    else {
      this.navCtrl.push('ItemCreatePage');

    }
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: Item) {
    this.navCtrl.push('ItemDetailPage', {
      item: item
    });
  }
}
