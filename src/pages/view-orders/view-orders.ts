import { Component,OnInit } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams,ToastController } from 'ionic-angular';
import { User } from '../../providers';
import {UpdateschedulePage} from '../updateschedule/updateschedule';
import { AlertController } from 'ionic-angular';
/**
 * Generated class for the ViewOrdersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-orders',
  templateUrl: 'view-orders.html',
})
export class ViewOrdersPage {
  requested:Boolean[]=[];
	 requestBody:any;
	 viewordersDetails:Array<any>=[];
     authKey = localStorage.getItem('authKey');
     from_date:any;
     to_date:any;
    userDetails = JSON.parse(localStorage.getItem("userDetails"));
requiredcancel:any[]=[];
suborders:Array<any>[]=[];
subordersview:Boolean[]=[]
suborderchecked:Array<any>[]=[]
subordercheckbox:Boolean[][]=[]

orderhighlights:Boolean[]=[]

  constructor(private alertCtrl:AlertController,public modalCtrl: ModalController,public navCtrl: NavController, public navParams: NavParams,public toastCtrl: ToastController,public user: User) {

// this.getViewOrdersDetails();
  }
ngOnInit(){
  // this.getViewOrdersDetails();
  console.log(this.userDetails)
      var today = new Date();
  var dd = String(today.getDate());
  var mm = String(today.getMonth() + 1); //January is 0!
  var yyyy = today.getFullYear();
  this.from_date = yyyy + '-' +"0"+ mm + '-' + dd;
  this.to_date= yyyy + '-' +"0"+ mm + '-' + dd;
  this.requestBody={
      "data": [{
   "user_id":this.userDetails.id,
   "auth_key":this.authKey,
   "from_date":this.from_date,
   "to_date":this.to_date,

  }]
}
  console.log(this.requestBody);
  this.user.Viewordes(this.requestBody).subscribe((resp:any)=>{

    if(resp.status== "success"){
      console.log(resp.data)
      if(resp.data.length != 0 && resp.data != 'Orders not found'){
        console.log(resp.data)


      // this.viewordersDetails =resp.data.reverse();


    for (let i = 0; i < resp.data.length; i++) {
      this.orderhighlights[i] = false
        var datsa ={"order_id":resp.data[i].id}
      this.user.getSuborder(datsa).subscribe(rs=>{
        console.log(rs)
        if(rs['status'] == "success"){
          for (let z = 0; z < rs['data'].length; z++) {
            console.log(rs['data'][z])
            if(rs['data'][z]['colorflag'] == 1){
              this.orderhighlights[i] =true
            }
          }
        }
      })
      this.requested[i]= false;
      this.requiredcancel[i] = 0;
      this.subordersview[i]=false;
      this.subordercheckbox[i]=[];
      this.subordercheck[i]=[]
var x = resp.data[i].delivered_date +' '+resp.data[i].delivered_time
console.log(x)
      // this.requiredcancel[i] =  new Date(resp.data[i].ordered_date).getTime() - new Date ().getTime()
      this.requiredcancel[i] =new Date ().getTime()   -  new Date(x).getTime()
this.requiredcancel[i] = ((this.requiredcancel[i]/60000)/60);

    }
      this.viewordersDetails =resp.data;
  }
    } else {
      let toast = this.toastCtrl.create({
        message: resp.message,
        duration: 3000,
        position: 'top'
      });
      toast.present();

    }
  });
}
  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewOrdersPage');
  }

  backopt()
  {
  this.navCtrl.push('TabsPage');

  }
printVal(){
  console.log(this.from_date)
    console.log(this.to_date)
    if(this.to_date==undefined || this.to_date=="undefined")
    {

        //  this.getBookingDetails();
         let toast = this.toastCtrl.create({
       message: 'Please Select To Date',
       duration: 3000,
       position: 'top'
     });
     toast.present();
    }
    else if(this.from_date==undefined || this.from_date=="undefined")
    {
let toast = this.toastCtrl.create({
       message: 'Please Select From Date',
       duration: 3000,
       position: 'top'
     });
     toast.present();
    }
    else
    {
         this.getViewOrdersDetails();
    }
  }
cancel(i,id){
// this.requested[i]=true

// var temp ={
//   order_id:id
// }
// this.user.getSuborder(temp).subscribe(Res=>{
//   console.log(Res)
//   this.suborders[i]=[];
//   // this.suborders[i]= Res.data;
//
// })
var temp = {
data:[{order_id:id,
auth_key:this.authKey}]
}
console.log(temp)
this.user.Cancelorder(temp).subscribe((resp:any)=>{
  console.log(resp)
  if(resp.status== "success"){
  for (let i = 0; i < resp.data.length; i++) {
    this.requested[i]= false

  }
  this.ngOnInit();

  } else {
    let toast = this.toastCtrl.create({
      message: resp.message,
      duration: 3000,
      position: 'top'
    });
    toast.present();
    //location.reload();
  }
});
}


subreshedule(id){
  console.log(this.userDetails)

  let profileModal = this.modalCtrl.create(UpdateschedulePage, { userid: id });
  profileModal.present();
//   const modal = this.modalCtrl.create(StarttripPage, {bookingDetails: item});
// modal.present();
}
   getViewOrdersDetails(){
     // let x = Date.today().add({days:-30});
     // console.log(x)
    this.requestBody={
        "data": [{
     "user_id":this.userDetails.id,
     "auth_key":this.authKey,
     "from_date":this.from_date,
     "to_date":this.to_date,

    }]
}
    console.log(this.requestBody);
    this.user.Viewordes(this.requestBody).subscribe((resp:any)=>{

      if(resp.status== "success"){
        console.log(resp.data)
        if(resp.data.length != 0){


        // this.requiredcancel[i] =  new Date(resp.data[i].ordered_date).getTime() - new Date ().getTime()
        this.viewordersDetails =resp.data;
        // this.subordersview(this.viewordersDetails.length).fill(true)
// fruits.fill("Kiwi",0);
// this.suborders[this.viewordersDetails.length].fill(true)
      for (let i = 0; i < resp.data.length; i++) {
        this.orderhighlights[i] = false
          var datsa ={"order_id":resp.data[i].id}
        this.user.getSuborder(datsa).subscribe(rs=>{
          console.log(rs)
          if(rs['status'] == "success"){
            for (let z = 0; z < rs['data'].length; z++) {
              console.log(rs['data'][z])
              if(rs['data'][z]['colorflag'] == 1){
                this.orderhighlights[i] =true
              }
            }
          }
        })
        this.requested[i]= false;
        this.requiredcancel[i] = 0;
        this.subordersview[i]=false;
        this.subordercheckbox[i]=[];
        this.subordercheck[i]=[]
var x = resp.data[i].delivered_date +' '+resp.data[i].delivered_time
console.log(x)
        // this.requiredcancel[i] =  new Date(resp.data[i].ordered_date).getTime() - new Date ().getTime()
        this.requiredcancel[i] =new Date ().getTime()   -  new Date(x).getTime()
this.requiredcancel[i] = ((this.requiredcancel[i]/60000)/60);
        // console.log(this.requiredcancel[i]);
        // console.log((this.requiredcancel[i]/60000)/60);
      }
      }
// console.log(this.subordersview)
      } else {
        let toast = this.toastCtrl.create({
          message: resp.message,
          duration: 3000,
          position: 'top'
        });
        toast.present();
        //location.reload();
      }
    });
  }
  //view sub order
viewsuborder(ind,id){
  console.log(id)
  this.suborders[ind]=[]
  var data ={"order_id":id}
  // for (let i = 0; i < this.viewordersDetails.length; i++) {
  //   console.log(i)
  //   this.subordersview[i]=false;
  //   this.subordercheckbox[ind][i]=false
  // }
  // this.subordersview[ind]=true;
  this.subordersview[ind]=!this.subordersview[ind];

if(this.subordersview[ind]){

  this.user.getSuborder(data).subscribe(res=>{
console.log(res)
this.suborders[ind] = res['data']
console.log(this.suborders[ind])
console.log(this.subordercheckbox[ind])
// for (let j = 0; j < this.suborders.length; j++) {
//   this.suborders[j]['deliver_time'] = this.suborders[j]['deliver_time'].slice(0, this.suborders[j]['deliver_time'].length-3);
// }
for (let i = 0; i < this.subordercheckbox[ind].length; i++) {
  console.log(i)
  // this.subordersview[i]=false;


  this.subordercheckbox[ind][i]=false
}
console.log(this.subordercheckbox[ind])
  })
}
}
subordercheck(e,id,ind,subind){
console.log(e)
// console.log(value)
if(e.checked == true){
  var value = this.subordercheck[ind].filter(data=>data === id)
  console.log(value)
  if(value.length == 0){
    this.subordercheckbox[ind][subind]=true
    this.subordercheck[ind].push(id)
  }
}
if(e.checked == false){
  var value = this.subordercheck[ind].findIndex(data=>data === id)
  console.log(value)

  if(value != -1 ){
    this.subordercheckbox[ind][subind]=false

    this.subordercheck[ind].splice(value,1)
  }
}
console.log(this.subordercheck[ind],ind)
}
subordercancel(ind){
  var title="cancel"
  var msg ="Do you Want to cancel ?"
this.presentAlertwithbutton(title,msg,ind)

console.log(  this.subordercheck[ind])
// var data ={
//   "suborder": this.subordercheck[ind]
// }
// console.log(data)
// this.user.Cancelsuborder(data).subscribe(res=>{
//   console.log(res)
//   var title = 'Request cancel'
//   var msg ='Successfully Request cancel'
// this.presentAlert(title,msg)
//   // this.ngOnInit()
// })
}
presentAlert(title,subtitle) {
  let alert = this.alertCtrl.create({
    title:title,
    subTitle:subtitle,
    // buttons: ['ok']
    buttons: [
       {
         text: 'OK',
         handler: () => {
           console.log('Confirm Okay');
           this.ngOnInit()

         }
       }
     ]
  });
  alert.present()
}
presentAlertwithbutton(title,subtitle,ind) {
  let alert = this.alertCtrl.create({
    title:title,
    subTitle:subtitle,
    // buttons: ['ok']
    buttons: [
       {
         text: 'Cancel',
         role: 'cancel',
         cssClass: 'secondary',
         handler: (blah) => {
           console.log('Confirm Cancel: blah');
         }
       }, {
         text: 'Okay',
         handler: () => {
           console.log(  this.subordercheck[ind])
           var data ={
             "suborder": this.subordercheck[ind]
           }
           console.log(data)
           this.user.Cancelsuborder(data).subscribe(res=>{
             console.log(res)
             var title = 'Request cancel'
             var msg ='Successfully Request cancel'
           this.presentAlert(title,msg)
             // this.ngOnInit()
           })
           console.log('Confirm Okay');
         }
       }
     ]
  });
  alert.present()
}
}
