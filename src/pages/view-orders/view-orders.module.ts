import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewOrdersPage } from './view-orders';
// import {UpdateschedulePage} from '../updateschedule/updateschedule';

@NgModule({
  declarations: [
    ViewOrdersPage,
    // UpdateschedulePage
  ],
  imports: [
    IonicPageModule.forChild(ViewOrdersPage),
  ],
  entryComponents:[]
})
export class ViewOrdersPageModule {}
