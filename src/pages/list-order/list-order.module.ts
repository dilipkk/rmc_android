import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { OrdersPage } from './list-order';

import { SignaturePadModule } from 'angular2-signaturepad';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
  
    SignaturePadModule,
    IonicPageModule.forChild(OrdersPage),
    TranslateModule.forChild()
  
  ],
  declarations: [
    OrdersPage
  ],
  entryComponents:[
    OrdersPage
  ]
})
export class OrdersPageModule {}
