import { Component, OnInit, ViewChild } from '@angular/core';
import { SignaturePad } from 'angular2-signaturepad/signature-pad';
import { IonicPage, NavController, ToastController, AlertController } from 'ionic-angular';
import { Orders } from '../../providers/orders/orders';
@IonicPage({

  segment: 'list-order'
})
@Component({
  selector: 'page-list-order',
  templateUrl: 'list-order.html',
  // styleUrls: ['list-order.scss'],
})
export class OrdersPage implements OnInit {
  public orders: Array<any> = [];
  public signature: Boolean[] = []
  public sign: Boolean[] = []
  driverid;
  strImage: any[] = [];
  suborderImg: any[] = [];
  Suborders: any[] = [];
  Subordersview: Boolean[] = [];
  subsignatures: Boolean[] = [];

  checksuborder: any[] = [];


  suborderlen: Array<any>[] = []
  @ViewChild(SignaturePad) signaturePad: SignaturePad;

  private signaturePadOptions: Object = { // passed through to szimek/signature_pad constructor
    'minWidth': 2,
    'canvasWidth': 800,
    'canvasHeight': 200,
    'penColor': "rgb(66, 133, 244)",
    'backgroundColor': 'rgb(255, 255, 255)',
  };

  constructor(private navctrl: NavController,
    private alert: AlertController,
    private orderservice: Orders, private toast: ToastController) {
    var id = localStorage.getItem('driver_id')
    this.driverid = JSON.parse(id)
  }
  y;
  ngOnInit() {
    this.listOrders()
  }

  listOrders1() {
    this.orders = []
    var id = {
      driver_id: this.driverid
    }

    this.orderservice.getorders(id).subscribe(res => {

      for (let i = 0; i < res['data']['orders'].length; i++) {
        let order: any = {};
        if (res['data'].orders[i]['order_status'] != 4) {
          order = res['data'].orders[i]

          for (let j = 0; j < this.orders.length; j++) {
            order.subOrder = [];
            var subdata = { "order_id": res['data'].orders[i].id }
            this.orderservice.getSuborder(subdata).subscribe(datas => {
              for (let k = 0; k < datas['data'].length; k++) {
                if (datas['data'][k]['status'] != "5") {
                  order.subOrder.push(datas['data'][k])
                }
              }
            })
          }
          this.orders.push(order)
          console.log("orders", this.orders);
        }
      }

    })


  }



  listOrders() {
    var id = {
      driver_id: this.driverid
    }
    this.orderservice.getorders(id).subscribe(res => {
      this.y = 0;
      if (res['status'] == 'success') {
        this.orders = []
        console.log(res)
        for (let i = 0; i < res['data']['orders'].length; i++) {
          if (res['data'].orders[i]['order_status'] != 4) {
            this.signature[i] = false;
            this.sign[i] = false;
            this.strImage[i] = undefined
            this.Subordersview[i] = false;
            this.Suborders[i] = []
            this.checksuborder[i] = []
            res['data'].orders[i]['index'] = i
            this.orders.push(res['data'].orders[i]);
            console.log("orders", this.orders);
            this.suborderlen[i] = []
            for (let j = 0; j < this.orders.length; j++) {
              var subdata = { "order_id": this.orders[j].id }
              this.orderservice.getSuborder(subdata).subscribe(datas => {

                for (let k = 0; k < datas['data'].length; k++) {

                  if (datas['data'][k]['status'] != "5") {

                    this.suborderlen[res['data'].orders[i]['index']].push(datas['data'][k])

                    if (this.suborderlen[i].length == 0) {

                    }

                  }
                  else {

                  }

                }
              })
            }

          }
        }
      }

    })

    for (let i = 0; i < this.orders.length; i++) {
      this.signature[i] = false;
      this.sign[i] = false;

    }
  }

  clickorders(order, ind) {
    var data = { "order_id": order.id }
    console.log(order)
    this.orderservice.getSuborder(data).subscribe(datas => {
      console.log(datas)
      this.Suborders[ind] = [];
      this.Suborders[ind] = datas['data'];
      this.checksuborder[ind] = []
      for (let g = 0; g < this.Suborders[ind].length; g++) {
        console.log(this.Suborders[ind])
        if (this.Suborders[ind][g]['status'] == "5") {
          this.checksuborder[ind].push(this.Suborders[ind][g]);
          console.log(this.checksuborder[ind])
        }
        console.log(this.Suborders[ind].length, this.checksuborder[ind].length)
      }
      for (let j = 0; j < this.Subordersview.length; j++) {
        this.Subordersview[j] = false
      }
      this.Subordersview[ind] = true
      console.log(this.Subordersview[ind], ind)
      console.log(this.Suborders[ind])
    })
  }
  ngAfterViewInit() {
    // this.signaturePad is now available
    // this.signaturePad.set('minWidth', 5); // set szimek/signature_pad options at runtime
    // this.signaturePad.clear(); // invoke functions from szimek/signature_pad API
  }
  subsignature(i, j) {
    console.log(i, j)
    for (let k = 0; k < this.Suborders.length; k++) {
      // this.subsignatures[k]=false;
      this.suborderImg[k] = undefined;

    }
    // this.subsignatures[j]=true
    this.subsignatures[j] = !this.subsignatures[j]

  }
  drawsubcorderComplete(ind) {
    this.suborderImg[ind] = this.signaturePad.toDataURL().replace(/^data:image\/[a-z]+;base64,/, "");
    console.log(this.suborderImg[ind]);
  }
  clicksubordersave(i, ind, data) {
    var datas = {
      sub_order_id: parseInt(data.id),
      driver_id: this.driverid,
      signature: this.suborderImg[ind]
    }
    console.log(i, ind, datas)
    this.orderservice.savesuborder(datas).subscribe(Res => {
      console.log(Res)
      this.presentAlert('Successfully  Delivered')
      // this.orders.splice(i, 1)
      this.Suborders[i].splice(ind, 1);
      this.listOrders();
    })
  }
  doRefresh(refresher) {
    this.listOrders()
    refresher.complete();
  }
  drawComplete(i) {
    // will be notified of szimek/signature_pad's onEnd event
    // console.log(this.signaturePad.toDataURL());
    this.strImage[i] = this.signaturePad.toDataURL().replace(/^data:image\/[a-z]+;base64,/, "");
    console.log(this.strImage);
  }

  drawStart() {
    // will be notified of szimek/signature_pad's onBegin event
    console.log('begin drawing');
  }

  clicksave(i, id) {
    console.log(this.signaturePad.toDataURL());
    var update = {
      order_id: id,
      driver_id: this.driverid,
      signature: this.strImage[i]

    }
    console.log(update)
    this.orderservice.updateorders(update).subscribe(res => {
      console.log(res)
      this.presentAlert('Successfully Delivered Orders Completed')
      // this.orders.splice(i, 1)
      this.sign[i] = false;
      this.listOrders();
    })
    // {"order_id":"180","driver_id":"7","signature":"fdsafdsafdsaasffsdafdsa"}
  }
  clicksign(i) {

    console.log(this.sign[i])



    if (this.sign[i] == false) {
      console.log(this.sign[i])
      this.sign[i] = true;
      console.log(this.sign[i])

    }
    else {
      console.log(this.sign[i])
      this.sign[i] = false;
      this.strImage[i] = undefined
      console.log(this.sign[i])

    }
  }
  clickclear(i) {
    this.strImage[i] = undefined
    this.signaturePad.clear(); // invoke functions from szimek/signature_pad API

  }
  clicksubclear(i) {
    this.suborderImg[i] = undefined
    this.signaturePad.clear();
  }
  logout() {
    console.log("logout");
    let alert = this.alert.create({
      message: "Are you sure to logout?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Logout',
          handler: data => {

            this.loggingOut();
          }
        }
      ]
    });
    alert.present();
  }

  loggingOut() {
    console.log("clearing local storage")
    localStorage.clear()
    this.navctrl.setRoot('LoginPage');

  }
  // async presentAlertConfirm() {
  //   const alert = await this.alert.create({
  //     header: 'Logout!',
  //     message: 'Do You Want To Logout ?',
  //     buttons: [
  //       {
  //         text: 'Cancel',
  //         role: 'cancel',
  //         cssClass: 'secondary',
  //         handler: (blah) => {
  //           console.log('Confirm Cancel: blah');
  //         }
  //       }, {
  //         text: 'Okay',
  //         handler: () => {
  //           console.log('Confirm Okay');
  //           localStorage.removeItem('driver_id')
  //           this.navctrl.navigateRoot('login')

  //         }
  //       }
  //     ]
  //   });

  //   await alert.present();
  // }

  async presentAlert(msg) {
    const alert = await this.toast.create({
      message: msg,
      duration: 2000,
      position: 'middle'
    });
    await alert.present();
  }
}
