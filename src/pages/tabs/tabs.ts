import { Component,ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage,Nav, NavController,AlertController } from 'ionic-angular';
import { User } from '../../providers';
import { Tab1Root, Tab2Root, Tab3Root } from '../';
import { LoginPage } from '../login/login'
@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  @ViewChild(Nav) nav: Nav;
  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";

  constructor(public navCtrl: NavController, public translateService: TranslateService,private alertCtrl: AlertController,public user: User) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE']).subscribe(values => {
      this.tab1Title = "Home";
      this.tab2Title = "Logout";
      this.tab3Title = "How to";
    });
  }

  logedout(){
    console.log("logout");
    let alert = this.alertCtrl.create({
      message: "Are you sure to logout?",
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Logout',
          handler: data => {
            this.loggingOut();
          }
        }
      ]
    });
    alert.present();
  }

  loggingOut(){
    localStorage.clear();
    this.navCtrl.setRoot('LoginPage');
    
    

  }


}
