
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import { ApiDriver } from '../apidriver/api'
/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class Orders {
  _user: any;

  constructor(public api: Api, public apid: ApiDriver) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  getorders(data) {
    // return  this.http.post(this.auth.api+ 'ebook/Login',data,{ headers:  this.auth.getHeaders() })
    // return  this.http.post(this.auth.api+ 'rmc/Api/getdriverorders',data,{ headers:  this.auth.getHeaders() })
    let seq = this.api.post('getdriverorders', data).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  getSuborder(data) {

    let seq = this.api.post('get_suborder', data).share();

    seq.subscribe((res: any) => {

      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  savesuborder(data) {

    let seq = this.api.post('complete_suborder', data).share();

    seq.subscribe((res: any) => {

      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  updateorders(data) {
    // return  this.http.post(this.auth.api+ 'rmc/Api/update_order',data,{ headers:  this.auth.getHeaders() })
    let seq = this.api.post('update_order', data).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  _loggedIn(resp) {
    this._user = resp.user;
  }


}
