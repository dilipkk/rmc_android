
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';

import { Injectable } from '@angular/core';

import { Api } from '../api/api';
import {ApiDriver } from '../apidriver/api'
/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
@Injectable()
export class User {
  _user: any;

  constructor(public api: Api,public apid:ApiDriver) { }

  /**
   * Send a POST request to our login endpoint with the data
   * the user entered on the form.
   */
  login(accountInfo: any) {
    let seq = this.api.post('login', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  driverlogin(data){
    let seq = this.apid.post('driver_logincheck', data).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
//get compan tdetails by phone Number

getcompanydetails(reqData: any) {
  let seq = this.api.post('get_company_details', reqData).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;

}
//
  /**
   * Create Customer
   */
  createCustomer(reqData: any) {
    let seq = this.api.post('create_customers', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;

  }
  //u[date customer]
  updateCustomer(reqData: any) {
    let seq = this.api.post('update_customer_info', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;

  }
  // get delivered Orders
  getdeliveredorders(reqData: any) {
    let seq = this.api.post('get_delivery_orders', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;

  }
  //
  //get companydetati;s by company name
  getcompanydetailsbyname(reqData: any) {
    let seq = this.api.post('get_companyname', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  //
//get delivered sub orders
getdeliveredsuborders(reqData: any) {
  let seq = this.api.post('get_delivered_suborder', reqData).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
//
get_delivery_orders(requestBody: any){
  let seq = this.api.post('get_completed_details', requestBody).share();

  seq.subscribe((res: any) => {
    console.log(res);
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
//
//get target details
getuserdetaills(requestBody: any) {
 let seq = this.api.post('get_target_details', requestBody).share();

 seq.subscribe((res: any) => {
   console.log(res);
 }, err => {
   console.error('ERROR', err);
 });

 return seq;
}
  /**
   * Create Calls
   */
  createCalls(reqData: any) {
    let seq = this.api.post('create_calls', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
/**
*Update Calls
*/
updateCalls(reqData: any) {
  console.log(reqData)
  // let seq = this.api.post('update_customer_info', reqData).share();
  let seq = this.api.post('update_calls', reqData).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    console.log(res);
    if (res.status == 'success') {
      this._loggedIn(res);
      console.log(res)
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}

/**
*Update zSchedule Calls
*/

getallcompany(){

  let seq = this.api.get('get_allcompany').share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    console.log(res);
    if (res.status == 'success') {
      this._loggedIn(res);
      console.log(res)
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
updatescheduleCalls(reqData: any) {
  console.log(reqData)
  let seq = this.api.post('update_calls', reqData).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    console.log(res);
    if (res.status == 'success') {
      this._loggedIn(res);
      console.log(res)
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}


  /**
   * Create Order
   */
  createOrder(reqData: any) {
    let seq = this.api.post('create_orders', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

    /**
   * ViewStatics
   */
  ViewStatics(reqData: any) {
    let seq = this.api.post('getstatics', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Send a POST request to our signup endpoint with the data
   * the user entered on the form.
   */
  signup(accountInfo: any) {
    let seq = this.api.post('signup', accountInfo).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**  create Order
  */

  CallList(reqData: any) {
    let seq = this.api.post('call_list', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

 /**  view Order
  */

  Viewordes(reqData: any) {
    let seq = this.api.post('view_orders', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
  Updateorders(reqData: any) {
    let seq = this.api.post('change_schedule', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }
Cancelorder(data){
  let seq = this.api.post('cancellorder', data).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
//set duborder
getSuborder(data){
  let seq = this.api.post('get_suborder', data).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
//cancel suborders

Cancelsuborder(data){
  let seq = this.api.post('cancellsuborder', data).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}

suborderscheduleupdate(reqData){

  let seq = this.api.post('change_schedule', reqData).share();

  seq.subscribe((res: any) => {
    // If the API returned a successful response, mark the user as logged in
    if (res.status == 'success') {
      this._loggedIn(res);
    } else {
    }
  }, err => {
    console.error('ERROR', err);
  });

  return seq;
}
  /**  Schedule Calls
  */

  SchedulesCalls(reqData: any) {
    let seq = this.api.post('create_orders', reqData).share();

    seq.subscribe((res: any) => {
      // If the API returned a successful response, mark the user as logged in
      if (res.status == 'success') {
        this._loggedIn(res);
      } else {
      }
    }, err => {
      console.error('ERROR', err);
    });

    return seq;
  }

  /**
   * Log the user out, which forgets the session
   */
  logout() {
    this._user = null;
  }

  /**
   * Process a login/signup response to store user data
   */
  _loggedIn(resp) {
    this._user = resp.user;
  }
}
