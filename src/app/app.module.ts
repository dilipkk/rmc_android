import { HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera/ngx';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { SignaturePadModule } from 'angular2-signaturepad';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import {LoginPage}from '../pages/login/login'
// import { AutoCompleteModule } from 'ionic2-auto-complete';
import { Items } from '../mocks/providers/items';
import { Settings, User, Api } from '../providers';
import {Orders } from '../providers/orders/orders';
import {ApiDriver} from '../providers/apidriver/api'
import { MyApp } from './app.component';
import {UpdateschedulePage} from '../pages/updateschedule/updateschedule';
import { IonicSelectableModule } from 'ionic-selectable';
import { OrdersPage } from '../pages/list-order/list-order';
import { OrdersPageModule} from  '../pages/list-order/list-order.module'



// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    UpdateschedulePage,
    // SignaturePadModule,
    // OrdersPage

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicSelectableModule,
    SignaturePadModule,
    OrdersPageModule,
    // IonicPageModule.forChild(HomePage)
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),


  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    // OrdersPage,
    UpdateschedulePage
  ],
  providers: [
    Api,
    Items,
    User,
    Orders,
    ApiDriver,
    Camera,
    SplashScreen,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule { }
